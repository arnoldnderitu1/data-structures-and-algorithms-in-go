# Lists
A sequence of elements. Each element can be connected to another with a link in a *forward* or *backward* direction. 

An element can have other payload properties. A list has a variable length. Elements can be added or removed more easily than in an array. 

> [list.go](Lists/list.go)
```
package main

import (
	"container/list"
	"fmt"
)

func main() {
	var intList list.List
	intList.PushBack(11)
	intList.PushBack(23)
	intList.PushBack(34)

	for element := intList.Front(); element != nil; element = element.Next() {

		fmt.Println(element.Value.(int))
	}
}
```

Above we are declaring and assigning `element` variable (`:=`) and iterating through the list using a `for` loop. We access the value of the `element` using the `Value` method.

To run the above program
```
go run $GOPATH/src/Lists/list.go
```
You should get an output such the one below

![list](Lists/list.png)




# Data Structures and Algorithms
## Data structure: 
The organization of data (in a computer's memory), in order to retrieve it quickly for processing.

## Classification of Data Structures
A data structure is choosen based on the problem type and the operations performed on the data.

For example a situation may require the use of various data types within a data structure, in such a situation we can choose heterogeneous data structures.

### Heterogeneous Data Structures
* Linked lists
* Ordered lists
* Unordered lists

### Linear Data Structures
* [Lists](Lists.md)
* Sets
* [Tuples](Tuples.md)
* Queues
* Stacks
* [Heaps](Heaps.md)

### Non-linear Data Structures
* Trees
* Tables
* Containers

### Homogenenous Data Structures
* Two-dimensional arrays
* Multi-dimensional arrays

### Dynamic Data Structures
* Dictionaries
* Tree sets
* Sequences

## Links
**Contents**: [Index](README.md)

**Next Section**: [Structural design patterns](Structural_design_patterns.md)
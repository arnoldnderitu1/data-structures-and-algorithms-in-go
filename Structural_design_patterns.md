# Structural Design Patterns
Describes the relationship between entities. Used to form large strucutures using classes and objects. 

Used to create a system with different system blocks in a flexible manner. 

## Gang of four
1. [Adapter](Adapter.md)
2. Bridge
3. Composite
4. Decorator
5. Facade
6. Flyweight
7. Private class data
8. Proxy
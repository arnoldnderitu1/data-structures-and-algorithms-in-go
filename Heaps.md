# Heaps
Based on the `heap` property. Used in selection, graph, and k-way merge algorithms. 

Operations such as finding, merging, insertion, key changes, and deleting are performed on heaps.

Part of the `container/heap` package in Go. According to the heap order (maximum heap) property, the value stored at each node is greater than or equal to its children.

If the order is descending, it is referred to as a maximum heap; otherwise, it's a minimum heap. 

The heap data structure is not a sorted data structure, but partially ordered. The following example shows how to use the `container/heap` package to create a heap data structure.

```
package main

import (
	"container/heap"
	"fmt"
)

// variable of integerHeap type
type IntegerHeap []int

// IntegerHeap method - get the length of integerHeap
func (iheap IntegerHeap) Len() int { return len(iheap) }

// IntegerHeap method - check if element of i index is less than j index
func (iheap IntegerHeap) Less(i, j int) bool { return iheap[i] < iheap[j] }

// IntegerHeap method - swaps the element of i to j index
func (iheap IntegerHeap) Swap(i, j int) { iheap[i], iheap[j] = iheap[j], iheap[i] }

func main() {
	var intHeap *IntegerHeap = &IntegerHeap{1, 4, 5}
	heap.Init(intHeap)
	heap.Push(intHeap, 2)
	fmt.Printf("minimum: %d\n", (*intHeap)[0])
	for intHeap.Len() > 0 {
		fmt.Printf("%d \n", heap.Pop(intHeap))
	}
}
```
> [Heaps](Heaps/heaps.go)

`IntegerHeap` has a `Push` method that pushes the item with the interface:

```
// IntegerHeap method - pushes the item
func (iheap *IntegerHeap) Push(heapintf interface{}) {
	*iheap = append(*iheap, heapintf.(int))
}

// IntegerHeap method - pops the item from the heap
func (iheap *IntegerHeap) Pop() interface{} {
	var n int
	var x1 int
	var previous IntegerHeap = *iheap
	n = len(previous)
	x1 = previous[n-1]
	*iheap = previous[0 : n-1]
	return x1
}

```
> [Heaps](Heaps/heaps.go)
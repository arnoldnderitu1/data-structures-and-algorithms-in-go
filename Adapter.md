# Adapter
Provides a wrapper with an interface required by the API client to link incompatible types and act as a translator between the two types.

Uses the interface of a class to be a class with another compatible interface.
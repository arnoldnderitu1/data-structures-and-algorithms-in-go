# Data Structures and Algorithms with Golang
This project is a look into how we can develop faster and more efficient GO code by learning data structures and algorithms.

We'll look into abstract data types, definition, and classification of data structures. And also performance analysis of algorithms and choosing appropriate data structures for structural design patterns.

## Index
-   1. Data Structures and Algorithms
    -   [Definition and Classification of Data Structures](Data_Structures_and_Algorithms.md)
        -   [Lists](Lists.md)
        -   [Tuples](Tuples.md)
        -   [Heaps](Heaps.md)
    -   [Structural design patterns](Structural_design_patterns.md)
    
- 2. Data Structures and Algorithms with Go
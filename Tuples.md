# Tuples
A finite sorted list of elements. It groups data. Tuples are typically ***immutable*** sequential collections.

> **Immutable**: unchanging over time or unable to be changed.

The element has related fields of different datatypes. The only way to modify a tuple is to change the fields. 

Operators such as `+` and `*` can be applied to tuples.

A database record is referred to as a tuple. In the following example, power series of integeres are calculated and the square and cube of the integer is returned as a tuple

```
package main

import (
	"fmt"
)

// get the power series of integer 'a' and return as tuple square and cube of the integer

func powerSeries(a int) (int, int) {
	return a * a, a * a * a
}

// main method
func main() {
	var square int
	var cube int
	square, cube = powerSeries(3)

	fmt.Println("Square ", square, "Cube", cube)
	fmt.Println(powerSeriesN(4))
	fmt.Println(powerSeriesE(5))
}
```
> [tuple](Tuples/tuple.go)

The tuples can be named in the `powerSeries` function as shown below

```
func powerSeriesN(a int) (square int, cube int) {
	square = a * a
	cube = square * a
	return
}
```

If there is an error, it can be passed with tuples as shown below

```
func powerSeriesE(a int) (int, int, error) {
	var square int = a * a
	var cube int = square * a
	return square, cube, nil
}
```

